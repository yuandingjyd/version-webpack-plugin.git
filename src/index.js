const fs = require('fs')
const path = require('path')
const {generateVersion, writeVersion, getCurrentVersion} = require("./assets/js/util");

class YdVersionWebpackPlugin {
    // 构造函数接受配置参数
    constructor (options) {
        this.options = options
    }
    apply (compiler) {
        compiler.hooks.environment.tap('YdVersionWebpackPlugin',
            compilation => {
                if (process.env.NODE_ENV === 'production') {
                    const publicDir = './public'
                    const file = publicDir + path.sep + 'version.json'
                    let version = getCurrentVersion()
                    if (this.options && this.options.version) {
                        version = this.options.version
                    } else {
                        if (this.options && this.options.generateVersion) {
                            version = this.options.generateVersion(version)
                        } else {
                            version = generateVersion(version)
                        }
                    }
                    const content = JSON.stringify({ version })
                    if (fs.existsSync(publicDir)) {
                        writeVersion(file, content)
                    } else {
                        fs.mkdir(publicDir, (err) => {
                            if (err) throw err
                            writeVersion(file, content)
                        })
                    }
                    console.log('版本号：', version)
                }
            })
    }
}

module.exports = YdVersionWebpackPlugin
