const fs = require('fs')
const path = require('path')

/**
 * 写入文件版本号
 * @param versionFileName
 * @param content
 */
const writeVersion = (versionFileName, content) => {
    // 写入文件
    fs.writeFile(versionFileName, content, (err) => {
        if (err) throw err
    })
}

/**
 * 生成新的版本号
 * @returns {string}
 */
function generateVersion (version = '') {
    const today = new Date()
    const month = today.getMonth() + 1
    const date = `${today.getFullYear()}${month.toString().padStart(2, '0')}${today
        .getDate()
        .toString()
        .padStart(2, '0')}`
    if (version) {
        const versionList = version.split('-')
        if (date === versionList[0]) {
            // 当天版本
            return `${date}-${(parseInt(versionList[1]) + 1).toString().padStart(3, '0')}`
        }
    }
    return `${date}-001`
}

/**
 * 获取当前项目的版本号
 * @returns {string}
 */
function getCurrentVersion () {
    let version = ''
    const publicDir = './public'
    const file = publicDir + path.sep + 'version.json'
    if (fs.existsSync(file)) {
        const data = fs.readFileSync(file)
        version = JSON.parse(data).version
    }
    return version
}

module.exports = {
    writeVersion,
    generateVersion,
    getCurrentVersion
}